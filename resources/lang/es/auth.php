<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed'   => 'Estas credenciales no coinciden con nuestros registros.',
    'throttle' => 'Demasiados intentos de acceso. Por favor intente nuevamente en :seconds segundos.',
    'empresa' => 'La empresa asociada a este usuario no se encuentra disponible',
    'usuario' => 'Este usuario no se encuentra disponible',
    'sin_cliente' => 'Este usuario no encuentra cliente disponible',
    'sin_rol' => 'Este usuario no encuentra rol disponible',

];
