<?php


return
    [
        'consulta_ok' => 'Consultado Correctamente',
        'delete_success' => 'Eliminado correctamente',
        'save_err' => 'error al guardar',
        'delete_err' => 'error al eliminar',
        'list_err' => 'error al listar',
        'success_modified' => 'modificado correctamente',
        

        'name_etapa_exist' => ' El nombre de la etapa ya existe',
        'save_success' => 'Guardado correctamente',
        'err_save_etapa' => 'error al guardar etapa',
        'err_edit_etapa' => 'error al editar etapa',
        'err_delete_etapa' => 'error al eliminar etapa',
        'err_list_etapa' => 'error al listar etapas',
        'name_etapa_exist' => 'El nombre de la etapa configuracion ya existe',
        'err_create_conf_etapa' => 'error al crear configuracion etapa',
        'err_edit_conf_etapa' => 'error al editar configuracion etapa',
        'err_delete_conf_etapa' => 'error al eliminar configuracion etapa',
        'err_list_conf_etapa' => 'error al listar configuracion etapa',
        'err_save_material_conf' => 'error al guardar material configuracion',
        'err_edit_material_conf' => 'error al editar material configuracion',
        'err_list_material' => 'error al listar materiales',
        'err_delete_material_conf' => 'error al eliminar material configuracion',
        'err_list_conf' => 'error al listar configuraciones',
        'err_list_conf_materials' => 'error al listar configuracion materiales',
        'clone_success' => 'clonado correctamente',
        'err_clone_conf_materials' => 'error al clonar configuracion materiales',
        'err_insert_info' => 'Error insertando información',
        'err_import_conf_materials' => 'error al importar configuracion materiales'

    ];
