<?php

namespace App\Http\Traits;

use DB;
use Config;
use Carbon\Carbon;
use Storage, Image;


trait ImagesTrait
{
    private $gbp_api = "https://app.grupobienpensado.co/api/convertir/url-to-gif";

    public static function bucket(){
        $bucket = "https://".config("filesystems.disks.s3.bucket").".s3.amazonaws.com/";
        return $bucket;
    }

    /*
        funcion para guardar la imagen con el porcentaje y la ruta deseada
        $img @binario Archivo de imágen
        $dimension @int porcentaje en que desea la imagen
        $ruta @string dirección donde estará alojada
        $mini @boolean guardar una versión mini
    */
    public function guardar_imagen_porcentaje($img, $dimension, $ruta, $mini = false, $dimension_mini = 100): array
    {
        try {
            // return $img;
            $imagen_file = Image::make($img);
            $imagen_file_ancho = $imagen_file->width();
            $imagen_file_alto = $imagen_file->height();

            //se obtiene el lado maximo de la imagen
            if ($imagen_file_ancho > $imagen_file_alto) {
                $imagen_max = $imagen_file_ancho;
            } else {
                $imagen_max = $imagen_file_alto;
            }

            //Se obtiene el porcentaje para cambiar el tamaño de la imagen para que no se distorcione
            $imagen_porcentaje = ($dimension / $imagen_max);

            //Solo se cambia de tamaño cuando alguno de los lados pasa la dimension requerida
            if ($imagen_porcentaje < 1) {
                $imagen_alto = $imagen_file_alto * $imagen_porcentaje;
                $imagen_ancho = $imagen_file_ancho * $imagen_porcentaje;
                $imagen = $imagen_file->resize($imagen_ancho, $imagen_alto);
            } else {
                $imagen = Image::make($img);
            }

            $ex = explode('/', $imagen->mime);
            $ext = end($ex);
            $nombre_aleatorio = uniqid(rand(), true) . str_replace(" ", "", microtime()) . ".$ext";
            $subruta = ($ruta != '' ? "$ruta/" : '') . $nombre_aleatorio;
            $subruta_mini = '';
            Storage::put($subruta, (string)$imagen->encode($ext));
            if ($mini) {
                $imagen_mini = $this->formatea_imagen($imagen, $dimension_mini);
                $nombre_aleatorio_mini = uniqid(rand(), true) . str_replace(" ", "", microtime()) . ".$ext";
                $subruta_mini = ($ruta != '' ? "$ruta/" : '') . $nombre_aleatorio_mini;
                Storage::put($subruta_mini, (string)$imagen_mini->encode($ext));
            }
            return [
                'estado' => true,
                'ruta' => $subruta,
                'ruta_mini' => $subruta_mini
            ];
        } catch (\Exception $e) {
            //return $e;
            return [
                'estado' => false,
                'ruta' => null,
                'ruta_mini' => null
            ];
        }
    }
    
    //Sirve para formatear la imágen antes de guardarla
    public function formatea_imagen($imagen, $porcentaje)
    {
        $imagen_file = Image::make($imagen);
        $imagen_file_ancho = $imagen_file->width();
        $imagen_file_alto = $imagen_file->height();

        if ($imagen_file_ancho > $imagen_file_alto) {
            $imagen_max = $imagen_file_ancho;
        } else {
            $imagen_max = $imagen_file_alto;
        }
        $imagen_porcentaje = ($porcentaje / $imagen_max);
        if ($imagen_porcentaje < 1) {
            $imagen_alto = $imagen_file_alto * $imagen_porcentaje;
            $imagen_ancho = $imagen_file_ancho * $imagen_porcentaje;
            $imagen = $imagen_file->resize($imagen_ancho, $imagen_alto);
        } else {
            $imagen = Image::make($imagen);
        }
        return $imagen;
    }


    /*
        funcion para retornar un imagen por defecto
        $tipo @Int
        tipo 1 = sin usuario, tipo 2 = sin producto, tipo 3 = sin producto v2, tipo 4 = tienda, 6= sin promocion, 7= sin cedis
    */

    public function no_imagen($tipo = null): string
    {
        switch ($tipo) {
            case 1:
                return "/img/no-imagen/sin_user.png";
                break;
            case 2:
                return "/img/sin_datos/mercado.svg";
                break;
            case 3:
                return "/img/sin_datos/mercado-1.svg";
                break;
            case 4:
                return "/img/no-imagen/sin_cliente.png"; // sin foto de la tienda
                break;
            case 5:
                return "/img/no-imagen/pedidos_manuales.png"; // avatar para usuario sin foto o para pedidos manuales
                break;
            case 6:
                return "/img/no-imagen/promociones.png";
                break;
            case 7:
                return "/img/no-imagen/sin_cedis.png";
                break;
            case 8:
                return "/img/modales/Grupo 25651.svg";
                break;
            default:
                return "/img/no-imagen/default.jpg";
                break;
        }
    }
    public function convertir_gif_video($rutaArchivo)
    {
        // $this->gbp_api;
        $bucket = config("filesystems.disks.s3.bucket");
        $img = 'http://'.$bucket.'.s3.amazonaws.com/'.$rutaArchivo ?? '';


       return $img2 = $this->gbp_api.'/'.$bucket.'/s3.amazonaws.com/'.$rutaArchivo;

    }
    public function getStoreFiles($file, $destinationPath = false) {
        try {
            if (isset($file)) {
                $response = [];
                $response['filename'] = $file->getClientOriginalName();
                $response['peso'] = $file->getSize();
                $response['ext'] = $file->getClientOriginalExtension();
                // $nombre_del_archivo = $response['filename'];
                $nombre_del_archivo = md5(uniqid('gbp-', true)) . "." . $response['ext'];

                return Storage::putFileAs($destinationPath, $file, $nombre_del_archivo);
            }
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }
}