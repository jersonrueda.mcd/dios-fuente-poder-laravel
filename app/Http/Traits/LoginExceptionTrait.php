<?php

namespace App\Http\Traits;


trait LoginExceptionTrait
{
    /**
     * Arroja una respuesta con excepción, en caso de ocurrir un error al momento de ingresar
     *
     * @param  int $errorType
     * @param  int $statusCodeError
     * @return \Illuminate\Http\Response
     */

    public function throwLoginErrorResponse(int $errorType = 0, int $statusCodeError = 400):\Illuminate\Http\Response
    {
        /**
        * @tipos:
        * - 0: Error no especificado
        * - 1: Credenciales incompletas
        * - 2: Usuario no existe en users
        * - 3: Usuario no existe en users
        * - 4: Cliente inactivo
        * - 5: Contraseña incorrecta
        */
        $errors = [
            0 => '¡Lo sentimos! Ha ocurrido un error inesperado al ingresar',
            1 => 'Alguno de los campos está vacío. Por favor, inténtelo nuevamente',
            2 => 'El usuario especificado no existe. Por favor, regístrese para continuar',
            3 => 'El usuario y contraseña no coinciden. Por favor, inténtelo nuevamente',
            4 => 'El usuario se encuentra inactivo',

        ];

        return response([
            "errors"=> ['username' => [$errors[$errorType]]]
        ], $statusCodeError);
    }
}
