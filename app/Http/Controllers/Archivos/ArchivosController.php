<?php

namespace App\Http\Controllers\Archivos;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class ArchivosController extends Controller
{
    public function eliminarArchivos(Request $request)
    {
        try {
            $data = 111;

            return $this->susccesResponse(['message' => 'Eliminados correctamente'], 200);

        } catch (\Exception $th) {
            return $this->capturar($th, "Ha ocurrido un error al intentar eliminar los archivos");
        }
    }
}

