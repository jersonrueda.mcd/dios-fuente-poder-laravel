<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\Auth\LoginRequest;
use Illuminate\Http\Request;
use App\Http\Traits\LoginExceptionTrait as LoginExceptions;
use App\Models\User;
use Tymon\JWTAuth\Facades\JWTAuth;


class LoginController extends Controller
{
    /**
     * Utiliza el trait de excepciones (App\Traits\sTrait)
     */

    use LoginExceptions;

    /**
     * Crea una nueva instancia del controlador
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
    }

    public function login(LoginRequest $request): \Illuminate\Http\Response
    {
        try {
            $credenciales = ['telefono' => $request->get('username'), 'password' => $request->get('password')];
            $usuario = User::where('telefono', $credenciales['telefono'])->exists();
            $usuarioActivo = User::where('telefono', $credenciales['telefono'])->where('estado', 1)->exists();
            /*
                Si no se encontró al usuario
            */
            if (!$usuario) {
                return $this->throwLoginErrorResponse(2, 400);
            }

            if (!$usuarioActivo) {
                return $this->throwLoginErrorResponse(4, 400);
            }

            if (!$token = JWTAuth::attempt($credenciales)) {
                return $this->throwLoginErrorResponse(3, 400);
            }
            return response(['message' => __("Sesión iniciada"), 'token' =>  $token], 200);
        } catch (\Exception $error) {
            return $this->capturar($error, __("error al ingresar"));
        }
    }

    public function logout(Request $request): \Illuminate\Http\Response
    {
        try {
            User::findOrFail(auth()->user()->id);
            JWTAuth::invalidate(JWTAuth::getToken());
            return response(['message' => __("sesión cerrada correctamente")], 200);
        } catch (\Exception $error) {
            return $this->capturar($error, __("error al terminar sesión"));
        }
    }
    public function listarPermisosUsuario(Request $request)
    {
        // try {
        //     $idUser = auth()->user()->id;

        //     //permisos tipo proyecto usuario
        //     $proyecto = Proyecto::find($request->idProyecto);
        //     $permisosProyecto = [];
        //     if (!is_null($proyecto)) {
        //         $proyectoUser = ProyectoUser::where(['id_user'=> $idUser, 'estado' => 1])->where('id_proyecto',$proyecto->id)->first();
        //         if (!is_null($proyectoUser)) {
        //             $userRoles = ProyectoUserRol::where('id_proyecto_user', $proyectoUser->id)->pluck('id_rol');
        //             $idGuardians = GuardianPermiso::where('id_tipo_proyecto', $proyecto->id_tipo_proyecto)->whereIn('id_rol',$userRoles)->pluck('id_guardian')->unique();
        //             $permisosProyecto = Guardian::whereIn('id',$idGuardians)->pluck('guardian');
        //         }
        //     }

        //     //permisos del sistema usuario
        //     $permisoSistema = PermisosTrait::permisosSistemaUser($idUser);

        //     $data['permisoProyecto'] = $permisosProyecto;
        //     $data['permisoSistema'] = $permisoSistema;

        //   return $this->susccesResponse(['message' => 'consultado correctamente', 'data' => $data]);

        // } catch(\Exception $e){
        //     return $this->capturar($e, 'Error al listar permisos usuario');
        // }
    }
}
