<?php
use Illuminate\Support\Facades\Route;

Route::prefix('archivos')->group(function() {

    $controller = 'Archivos\ArchivosController';

	Route::get('eliminar-archivos', "$controller@eliminarArchivos");
});