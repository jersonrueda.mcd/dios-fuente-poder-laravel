<?php
use Illuminate\Support\Facades\Route;

Route::namespace('Auth')->group(function ()
{
    Route::post('login', 'LoginController@login');
    Route::post('logout', 'LoginController@logout');

    Route::get('usuario-permisos', 'LoginController@listarPermisosUsuario');

});